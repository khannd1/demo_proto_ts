```
npm init
npm install --save-dev typescript ts-loader webpack html-webpack-plugin webpack-cli webpack-dev-server
npm install --save google-protobuf grpc-web
```

Generate proto commnad:
```
protoc -I=. api/*.proto --js_out=import_style=commonjs:$PWD/src/  --grpc-web_out=import_style=typescript,mode=grpcwebtext:$PWD/src
```


Run dev(support hot reload):
```
npm run dev
```

Build:
```
npm run build
```