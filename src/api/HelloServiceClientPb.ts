/**
 * @fileoverview gRPC-Web generated client stub for hello
 * @enhanceable
 * @public
 */

// GENERATED CODE -- DO NOT EDIT!


/* eslint-disable */
// @ts-nocheck


import * as grpcWeb from 'grpc-web';

import * as api_hello_pb from '../api/hello_pb';


export class HelloServiceClient {
  client_: grpcWeb.AbstractClientBase;
  hostname_: string;
  credentials_: null | { [index: string]: string; };
  options_: null | { [index: string]: any; };

  constructor (hostname: string,
               credentials?: null | { [index: string]: string; },
               options?: null | { [index: string]: any; }) {
    if (!options) options = {};
    if (!credentials) credentials = {};
    options['format'] = 'text';

    this.client_ = new grpcWeb.GrpcWebClientBase(options);
    this.hostname_ = hostname;
    this.credentials_ = credentials;
    this.options_ = options;
  }

  methodDescriptorSayHello = new grpcWeb.MethodDescriptor(
    '/hello.HelloService/SayHello',
    grpcWeb.MethodType.UNARY,
    api_hello_pb.HelloRequest,
    api_hello_pb.HelloReply,
    (request: api_hello_pb.HelloRequest) => {
      return request.serializeBinary();
    },
    api_hello_pb.HelloReply.deserializeBinary
  );

  sayHello(
    request: api_hello_pb.HelloRequest,
    metadata: grpcWeb.Metadata | null): Promise<api_hello_pb.HelloReply>;

  sayHello(
    request: api_hello_pb.HelloRequest,
    metadata: grpcWeb.Metadata | null,
    callback: (err: grpcWeb.RpcError,
               response: api_hello_pb.HelloReply) => void): grpcWeb.ClientReadableStream<api_hello_pb.HelloReply>;

  sayHello(
    request: api_hello_pb.HelloRequest,
    metadata: grpcWeb.Metadata | null,
    callback?: (err: grpcWeb.RpcError,
               response: api_hello_pb.HelloReply) => void) {
    if (callback !== undefined) {
      return this.client_.rpcCall(
        this.hostname_ +
          '/hello.HelloService/SayHello',
        request,
        metadata || {},
        this.methodDescriptorSayHello,
        callback);
    }
    return this.client_.unaryCall(
    this.hostname_ +
      '/hello.HelloService/SayHello',
    request,
    metadata || {},
    this.methodDescriptorSayHello);
  }

}

