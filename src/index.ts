import * as pb from './api/hello_pb'



var ws = new WebSocket('ws://localhost:8080/ws');
ws.onmessage = function (e) {
  console.log('data', e.data);
}
ws.onopen = function () {
  console.log('open');
}

// Demo serialize and deserialize protobuf
var req = new pb.HelloRequest();
req.setName('hello, wwworld');
var buf = req.serializeBinary();
var req2 = pb.HelloRequest.deserializeBinary(buf);
console.log(req2.getName());


// Demo get/set data to html element
(document.getElementById("msg") as any).value = req2.getName();